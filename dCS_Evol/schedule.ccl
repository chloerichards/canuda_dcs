# Schedule definitions for thorn EdGB_dec_Evol
# $Header:$

# Schedule EdGB_dec evolution if evolution_method=EdGB_dec_Evol
if (CCTK_EQUALS (evolution_method, "EdGB_dec_Evol")) {

  STORAGE: rhs_phi_group[1] rhs_Kphi_group[1]
  STORAGE: fPhi_coupling_group[1]
  STORAGE: conf_fac_WW[1] hmetric[1] hcurv[1] trk[1] gammat[1]
  STORAGE: EdGB_dec_Invariant[3]

  ## HW: added
  if( calculate_EdGB_Tmunu )
  {
    STORAGE: EdGB_dec_TmnEff_Sca_gfs[1] EdGB_dec_TmnEff_Vec_gfs[1] EdGB_dec_TmnEff_Ten_gfs[1]
  }

  ## HW: took off densities
  ## if( calculate_densities )
  ## {
  ##   STORAGE: densities_EdGB_dec[3]
  ## }

  if( calculate_norms )
  {
    STORAGE: EdGB_dec_norm_gfs[3]
  }

schedule EdGB_dec_symmetries at BASEGRID
{
  LANG: Fortran
  OPTIONS: Global
} "Register symmetries of the rhs grid functions"


schedule EdGB_dec_zero_rhs at BASEGRID after EdGB_dec_symmetries
{
  LANG: Fortran
} "set all rhs functions to zero to prevent spurious nans"

# MoL: registration
schedule EdGB_dec_RegisterVars in MoL_Register
{
  LANG: C
  OPTIONS: META
} "Register variables for MoL"

## Transform from ADM to BSSN variables in preparation for RHS calculation
schedule EdGB_dec_adm2wbssn in MoL_PreStep
{
  LANG: Fortran
  OPTIONS: Local
  SYNC: gammat
} "Convert initial data into WBSSN variables"

schedule EdGB_dec_WBSSN_Boundaries in MoL_PreStep after EdGB_dec_adm2wbssn
{
  LANG: Fortran
  OPTIONS: LEVEL
  SYNC: ADMBase::lapse
  SYNC: ADMBase::shift
  SYNC: EdGB_dec_Evol::conf_fac_WW
  SYNC: EdGB_dec_Evol::hmetric
  SYNC: EdGB_dec_Evol::hcurv
  SYNC: EdGB_dec_Evol::trk
  SYNC: EdGB_dec_Evol::gammat
} "Register boundary enforcement in MoL"

schedule GROUP ApplyBCs as EdGB_dec_WBSSN_ApplyBCs in MoL_PreStep after EdGB_dec_WBSSN_Boundaries
{
} "Apply boundary conditions"


# Calculate GB invariant in preparation for RHS calculation
schedule EdGB_dec_calc_GBinv in MoL_PreStep after EdGB_dec_WBSSN_ApplyBCs
{
  LANG: Fortran
  OPTIONS: Local
  SYNC: EdGB_dec_Invariant
} "Compute GB invariant"

schedule EdGB_dec_GBinv_Boundaries in MoL_PreStep after EdGB_dec_calc_GBinv
{
  LANG: Fortran
  OPTIONS: LEVEL
  SYNC: EdGB_dec_Evol::EdGB_dec_Invariant
} "Register boundary enforcement in MoL"

schedule GROUP ApplyBCs as EdGB_dec_GBinv_ApplyBCs in MoL_PreStep after EdGB_dec_GBinv_Boundaries
{
} "Apply boundary conditions"

schedule EdGB_dec_GBinv_Boundaries in MoL_PostStep after EdGB_dec_calc_GBinv
{
  LANG: Fortran
  OPTIONS: LEVEL
  SYNC: EdGB_dec_Evol::EdGB_dec_Invariant
} "Register boundary enforcement in MoL"

schedule GROUP ApplyBCs as EdGB_dec_GBinv_ApplyBCs in MoL_PostStep after EdGB_dec_GBinv_Boundaries
{
} "Apply boundary conditions"



# MoL: compute source terms, etc
schedule EdGB_dec_calc_rhs in MoL_CalcRHS as EdGB_dec_CalcRHS
{
  LANG: Fortran
} "Register RHS calculation for MoL"


# Condition for cartesian-only boundary implementation
if (!z_is_radial) {
  schedule EdGB_dec_calc_rhs_bdry in MoL_CalcRHS as EdGB_dec_CalcRHS_Bdry \
           after EdGB_dec_CalcRHS
  {
  LANG: Fortran
  } "Register boundary RHS calculation for MoL"
}

# Condition for multipatch boundary implementation
if (z_is_radial) {
  schedule EdGB_dec_calc_rhs_bdry_sph in MoL_RHSBoundaries as EdGB_dec_CalcRHS_Bdry_Sph
  {
    LANG: Fortran
  } "MoL boundary RHS calculation in spherical coordinates"
}

schedule EdGB_dec_Boundaries in MoL_PostStep
{
  LANG: Fortran
  OPTIONS: LEVEL
  SYNC: EdGB_dec_Base::phi_group
  SYNC: EdGB_dec_Base::Kphi_group
} "Register boundary enforcement in MoL"

schedule GROUP ApplyBCs as EdGB_dec_ApplyBCs in MoL_PostStep after EdGB_dec_Boundaries
{
} "Apply boundary conditions"

## HW: added
if( calculate_EdGB_Tmunu )
{

  schedule GROUP EdGB_dec_calc_TmnEff_Group IN MoL_PreStep AFTER EdGB_dec_GBinv_ApplyBCs
  {
  } "Schedule calculation of effective energy-momentum tensor"

   schedule EdGB_dec_calc_TmnEff IN EdGB_dec_calc_TmnEff_Group
   {
     LANG: Fortran
     OPTIONS: Local
     SYNC: EdGB_dec_Evol::EdGB_dec_TmnEff_Sca_gfs
     SYNC: EdGB_dec_Evol::EdGB_dec_TmnEff_Vec_gfs
     SYNC: EdGB_dec_Evol::EdGB_dec_TmnEff_Ten_gfs
   } "Compute effective energy-momentum tensor"

   schedule EdGB_dec_calc_TmnEff_boundaries IN EdGB_dec_calc_TmnEff_Group AFTER EdGB_dec_calc_TmnEff
   {
     LANG: Fortran
     OPTIONS: LOCAL
     SYNC: EdGB_dec_Evol::EdGB_dec_TmnEff_Sca_gfs
     SYNC: EdGB_dec_Evol::EdGB_dec_TmnEff_Vec_gfs
     SYNC: EdGB_dec_Evol::EdGB_dec_TmnEff_Ten_gfs
   } "Enforce symmetry BCs for effective energy-momentum tensor"

   schedule GROUP ApplyBCs as EdGB_dec_calc_TmnEff_ApplyBCs IN EdGB_dec_calc_TmnEff_Group AFTER EdGB_dec_calc_TmnEff_boundaries
   {
   } "Apply boundary conditions"

}

## HW: took off density calculation
## # compute densities
## if( calculate_densities )
## {
##   schedule EdGB_dec_densities at CCTK_ANALYSIS
##   {
##     LANG: Fortran
##     OPTIONS: Local
##     SYNC: EdGB_dec_Evol::densities_EdGB_dec
##   } "Compute constraints"
## 
##   schedule EdGB_dec_densities_boundaries at CCTK_ANALYSIS after EdGB_dec_densities
##   {
##     LANG: Fortran
##     OPTIONS: LEVEL
##     SYNC: EdGB_dec_Evol::densities_EdGB_dec
##   } "Enforce symmetry BCs in constraint computation"
## 
##   schedule GROUP ApplyBCs as EdGB_dec_densities_ApplyBCs at CCTK_ANALYSIS after EdGB_dec_densities_boundaries
##   {
##   } "Apply boundary conditions"
## 
## }

if( calculate_norms )
{
  schedule EdGB_dec_norms at CCTK_ANALYSIS
  {
    LANG: Fortran
    OPTIONS: Local
    SYNC: EdGB_dec_Evol::EdGB_dec_norm_gfs
  } "Compute constraints"

  schedule EdGB_dec_norms_boundaries at CCTK_ANALYSIS after EdGB_dec_norms
  {
    LANG: Fortran
    OPTIONS: LEVEL
    SYNC: EdGB_dec_Evol::EdGB_dec_norm_gfs
  } "Enforce symmetry BCs in constraint computation"

  schedule GROUP ApplyBCs as EdGB_dec_norms_ApplyBCs at CCTK_ANALYSIS after EdGB_dec_norms_boundaries
  {
  } "Apply boundary conditions"

}

} # end if evolution_method="EdGB_dec_Evol"
