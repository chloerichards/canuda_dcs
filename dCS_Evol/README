Canuda Thorn : EdGB_dec_Evol
Author(s)    : Helvi Witek (hwitek@illinois.edu), Matthew Elley
Maintainer(s): Helvi Witek, Matthew Elley, Hector O. Silva
Licence      : LGPLv2+
--------------------------------------------------------------------------

1. Purpose
Evolution in EdGB in the decoupling limit, i.e., evolution of the scalar field equation in a vacuum GR background.


2. Documentation and references
For the formulation check
- shift-symmetric sGB for single, spherically symmetric BHs and Oppenheimer-Snyder collapse:
https://arxiv.org/abs/1610.09168
https://arxiv.org/abs/1612.08184
- shift-symmetric sGB for rotating BHs and BH binaries:
https://arxiv.org/abs/1810.05177

3. Additional comments
NOTE:   In contrast to the 2018 paper we absorbed the coefficient of $f(\Phi) = 1/8 \Phi$ into the equations. 
        So, in the code we have $f(\Phi) = \Phi$.


