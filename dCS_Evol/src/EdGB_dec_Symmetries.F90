! EdGB_dec_Basegrid.F90: Register symmetries of the grid functions
!
!=============================================================================

#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

subroutine EdGB_dec_symmetries( CCTK_ARGUMENTS )

  implicit none
  DECLARE_CCTK_ARGUMENTS
  DECLARE_CCTK_PARAMETERS

  CCTK_INT ierr

  !=== Scalar field rhs ====
  call SetCartSymVN( ierr, cctkGH, (/ 1, 1, 1/), "EdGB_dec_Evol::rhs_Phi_gf" )
  call SetCartSymVN( ierr, cctkGH, (/ 1, 1, 1/), "EdGB_dec_Evol::rhs_KPhi_gf" )

  !=== WBSSN variables =====
  call SetCartSymVN( ierr, cctkGH, (/ 1, 1, 1/), "EdGB_dec_Evol::WW_gf" )

  call SetCartSymVN( ierr, cctkGH, (/ 1, 1, 1/), "EdGB_dec_Evol::hxx" )
  call SetCartSymVN( ierr, cctkGH, (/-1,-1, 1/), "EdGB_dec_Evol::hxy" )
  call SetCartSymVN( ierr, cctkGH, (/-1, 1,-1/), "EdGB_dec_Evol::hxz" )
  call SetCartSymVN( ierr, cctkGH, (/ 1, 1, 1/), "EdGB_dec_Evol::hyy" )
  call SetCartSymVN( ierr, cctkGH, (/ 1,-1,-1/), "EdGB_dec_Evol::hyz" )
  call SetCartSymVN( ierr, cctkGH, (/ 1, 1, 1/), "EdGB_dec_Evol::hzz" )

  call SetCartSymVN( ierr, cctkGH, (/ 1, 1, 1/), "EdGB_dec_Evol::axx" )
  call SetCartSymVN( ierr, cctkGH, (/-1,-1, 1/), "EdGB_dec_Evol::axy" )
  call SetCartSymVN( ierr, cctkGH, (/-1, 1,-1/), "EdGB_dec_Evol::axz" )
  call SetCartSymVN( ierr, cctkGH, (/ 1, 1, 1/), "EdGB_dec_Evol::ayy" )
  call SetCartSymVN( ierr, cctkGH, (/ 1,-1,-1/), "EdGB_dec_Evol::ayz" )
  call SetCartSymVN( ierr, cctkGH, (/ 1, 1, 1/), "EdGB_dec_Evol::azz" )

  call SetCartSymVN( ierr, cctkGH, (/ 1, 1, 1/), "EdGB_dec_Evol::tracek" )

  call SetCartSymVN( ierr, cctkGH, (/-1, 1, 1/), "EdGB_dec_Evol::gammatx" )
  call SetCartSymVN( ierr, cctkGH, (/ 1,-1, 1/), "EdGB_dec_Evol::gammaty" )
  call SetCartSymVN( ierr, cctkGH, (/ 1, 1,-1/), "EdGB_dec_Evol::gammatz" )

  !=== GB invariant ========
  call SetCartSymVN( ierr, cctkGH, (/ 1, 1, 1/), "EdGB_dec_Evol::GBinv_gf" )

  !! HW: added
  !=== Effective Tmn =======
  call SetCartSymVN( ierr, cctkGH, (/ 1, 1, 1/), "EdGB_dec_Evol::rhoSGB_gf")
  call SetCartSymVN( ierr, cctkGH, (/ 1, 1, 1/), "EdGB_dec_Evol::jrSGB_gf")

  call SetCartSymVN( ierr, cctkGH, (/-1, 1, 1/), "EdGB_dec_Evol::jxSGB_gf")
  call SetCartSymVN( ierr, cctkGH, (/ 1,-1, 1/), "EdGB_dec_Evol::jySGB_gf")
  call SetCartSymVN( ierr, cctkGH, (/ 1, 1,-1/), "EdGB_dec_Evol::jzSGB_gf")

  call SetCartSymVN( ierr, cctkGH, (/ 1, 1, 1/), "EdGB_dec_Evol::SxxSGB_gf")
  call SetCartSymVN( ierr, cctkGH, (/-1,-1, 1/), "EdGB_dec_Evol::SxySGB_gf")
  call SetCartSymVN( ierr, cctkGH, (/-1, 1,-1/), "EdGB_dec_Evol::SxzSGB_gf")
  call SetCartSymVN( ierr, cctkGH, (/ 1, 1, 1/), "EdGB_dec_Evol::SyySGB_gf")
  call SetCartSymVN( ierr, cctkGH, (/ 1,-1,-1/), "EdGB_dec_Evol::SyzSGB_gf")
  call SetCartSymVN( ierr, cctkGH, (/ 1, 1, 1/), "EdGB_dec_Evol::SzzSGB_gf")


  !! HW: take off old densities
  !=== Density variables ===
  !! call SetCartSymVN( ierr, cctkGH, (/ 1, 1, 1/), "EdGB_dec_Evol::rhoSF_gf" )
  !! call SetCartSymVN( ierr, cctkGH, (/ 1, 1, 1/), "EdGB_dec_Evol::rhoGB_gf" )
  !! call SetCartSymVN( ierr, cctkGH, (/ 1, 1, 1/), "EdGB_dec_Evol::jrSF_gf" )
  !! call SetCartSymVN( ierr, cctkGH, (/ 1, 1, 1/), "EdGB_dec_Evol::jrGB_gf" )

  !=== Norms ===============
  call SetCartSymVN( ierr, cctkGH, (/ 1, 1, 1/), "EdGB_dec_Evol::norm_gd3d_gf" )
  call SetCartSymVN( ierr, cctkGH, (/ 1, 1, 1/), "EdGB_dec_Evol::norm_gd4d_gf" )
  call SetCartSymVN( ierr, cctkGH, (/ 1, 1, 1/), "EdGB_dec_Evol::norm_TabSF_gf" )


end subroutine EdGB_dec_symmetries
